const cv = document.getElementById('tools_sketch')
cv.addEventListener('mousedown', activateDraw);
cv.addEventListener('touchstart', activateDraw);
cv.addEventListener('mousemove', draw);
cv.addEventListener('touchmove', draw);
cv.addEventListener('mouseup', deactivateDrawAndPredict);
cv.addEventListener('mouseleave', deactivateDrawAndPredict);
cv.addEventListener('touchend', deactivateDrawAndPredict);

const ctxSketch = cv.getContext('2d');
const ctxCenter = document.getElementById('input-canvas-centercrop').getContext('2d');
const ctxScaled = document.getElementById('input-canvas-scaled').getContext('2d');

const model = new KerasJS.Model({
  filepaths: {
    model: 'data/model.json',
    weights: 'data/model_weights.buf',
    metadata: 'data/model_metadata.json'
  },
  gpu: false
})

var drawing = false;
var strokes = [];

function activateDraw(e) {
  drawing = true;
  strokes.push([]);
  let points = strokes[strokes.length - 1]
  points.push(getCoordinates(e))
}

function draw(e) {
  if (!drawing) return

  ctxSketch.lineWidth = 20
  ctxSketch.lineJoin = ctxSketch.lineCap = 'round'
  ctxSketch.strokeStyle = '#393E46'
  ctxSketch.clearRect(0, 0, ctxSketch.canvas.width, ctxSketch.canvas.height)
  let points = strokes[strokes.length - 1]
  points.push(getCoordinates(e))
  // draw individual strokes
  for (let s = 0, slen = strokes.length; s < slen; s++) {
    points = strokes[s]
    let p1 = points[0]
    let p2 = points[1]
    ctxSketch.beginPath()
    ctxSketch.moveTo(...p1)
    // draw points in stroke
    // quadratic bezier curve
    for (let i = 1, len = points.length; i < len; i++) {
      ctxSketch.quadraticCurveTo(...p1, ...getMidpoint(p1, p2))
      p1 = points[i]
      p2 = points[i + 1]
    }
    ctxSketch.lineTo(...p1)
    ctxSketch.stroke()
  }
}

function deactivateDrawAndPredict(e) {
  if (!drawing) return
  drawing = false

  //load()
}

function clearCanvas() {
  ctxSketch.clearRect(0, 0, ctxSketch.canvas.width, ctxSketch.canvas.height)
  ctxCenter.clearRect(0, 0, ctxCenter.canvas.width, ctxCenter.canvas.height)
  ctxScaled.clearRect(0, 0, ctxScaled.canvas.width, ctxScaled.canvas.height)
  drawing = false
  strokes = []
}

function load() {
  // center crop
  const imageDataCenterCrop = centerCrop(ctxSketch.getImageData(0, 0, ctxSketch.canvas.width, ctxSketch.canvas.height))
  ctxCenter.canvas.width = imageDataCenterCrop.width
  ctxCenter.canvas.height = imageDataCenterCrop.height
  ctxCenter.putImageData(imageDataCenterCrop, 0, 0)

  // scaled to 28 x 28
  ctxScaled.save()
  ctxScaled.scale(28 / ctxCenter.canvas.width, 28 / ctxCenter.canvas.height)
  ctxScaled.clearRect(0, 0, ctxCenter.canvas.width, ctxCenter.canvas.height)
  ctxScaled.drawImage(document.getElementById('input-canvas-centercrop'), 0, 0)
  const imageDataScaled = ctxScaled.getImageData(0, 0, ctxScaled.canvas.width, ctxScaled.canvas.height)
  ctxScaled.restore()

  // process image data for model input
  const {
    data
  } = imageDataScaled
  //console.log(data);
  const input = new Float32Array(784)
  for (let i = 0, len = data.length; i < len; i += 4) {
    input[i / 4] = data[i + 3] / 255
  }

  model.ready()
    .then(() => {
      const inputData = {
        input: input
      }

      // make predictions
      return model.predict(inputData)
    })
    .then(outputData => {
      var max = 0,
        out;
      for (var key in outputData.output) {
        if (outputData.output.hasOwnProperty(key) && outputData.output[key] > max) {
          max = outputData.output[key];
          out = key;
        }
      }
      alert(out);
    })
    .catch(err => {
      //console.log(err);
    })
}


/**
 * Centers and crops canvas ImageData based on alpha channel.
 * @param {ImageData} imageData
 * @returns {ImageData}
 */
function centerCrop(imageData) {
  const {
    data,
    width,
    height
  } = imageData
  let [xmin, ymin] = [width, height]
  let [xmax, ymax] = [-1, -1]
  for (let i = 0; i < width; i++) {
    for (let j = 0; j < height; j++) {
      const idx = i + j * width
      if (data[4 * idx + 3] > 0) {
        if (i < xmin) xmin = i
        if (i > xmax) xmax = i
        if (j < ymin) ymin = j
        if (j > ymax) ymax = j
      }
    }
  }

  // add a little padding
  xmin -= 20
  xmax += 20
  ymin -= 20
  ymax += 20

  // make bounding box square
  let [widthNew, heightNew] = [xmax - xmin + 1, ymax - ymin + 1]
  if (widthNew < heightNew) {

    const halfBefore = Math.floor((heightNew - widthNew) / 2)
    const halfAfter = heightNew - widthNew - halfBefore
    xmax += halfAfter
    xmin -= halfBefore
  } else if (widthNew > heightNew) {

    const halfBefore = Math.floor((widthNew - heightNew) / 2)
    const halfAfter = widthNew - heightNew - halfBefore
    ymax += halfAfter
    ymin -= halfBefore
  }

  widthNew = xmax - xmin + 1
  heightNew = ymax - ymin + 1
  let dataNew = new Uint8ClampedArray(widthNew * heightNew * 4)
  for (let i = xmin; i <= xmax; i++) {
    for (let j = ymin; j <= ymax; j++) {
      if (i >= 0 && i < width && j >= 0 && j < height) {
        const idx = i + j * width
        const idxNew = i - xmin + (j - ymin) * widthNew
        dataNew[4 * idxNew + 3] = data[4 * idx + 3]
      }
    }
  }

  return new ImageData(dataNew, widthNew, heightNew)
}


function getCoordinates(e) {
  let {
    clientX,
    clientY
  } = e
  // for touch event
  if (e.touches && e.touches.length) {
    clientX = e.touches[0].clientX
    clientY = e.touches[0].clientY
  }
  const {
    left,
    top
  } = e.target.getBoundingClientRect()
  const [x, y] = [clientX - left, clientY - top]
  return [x, y]
}


function getMidpoint(p1, p2) {
  const [x1, y1] = p1
  const [x2, y2] = p2
  return [x1 + (x2 - x1) / 2, y1 + (y2 - y1) / 2]
}
